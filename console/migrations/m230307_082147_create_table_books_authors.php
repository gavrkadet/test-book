<?php

use yii\db\Migration;

/**
 * Class m230307_082147_create_table_relation_books_authors
 */
class m230307_082147_create_table_books_authors extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('books_authors', [
            'book_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('books_authors',
            'books_authors',
            ['book_id', 'author_id']);

        $this->addForeignKey(
            'fk_bookID1',
            'books_authors',
            'book_id',
            'books',
            'id',
            'CASCADE',
            'CASCADE',
        );

        $this->addForeignKey(
            'fk_authorID',
            'books_authors',
            'author_id',
            'authors',
            'id',
            'CASCADE',
            'CASCADE',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_authorID', 'books_authors');
        $this->dropForeignKey('fk_bookID1', 'books_authors');
        $this->dropPrimaryKey('books_authors', 'books_authors');
        $this->dropTable('books_authors');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230307_082147_create_table_books_authors cannot be reverted.\n";

        return false;
    }
    */
}
