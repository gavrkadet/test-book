<?php

use yii\db\Migration;

/**
 * Class m230307_074534_create_table_books_statuses
 */
class m230307_074534_create_table_statuses extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('statuses', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'fk_status',
            'books',
            'status_id',
            'statuses',
            'id',
            'CASCADE',
            'CASCADE',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_status', 'books');
        $this->dropTable('statuses');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230307_074534_create_table_statuses cannot be reverted.\n";

        return false;
    }
    */
}
