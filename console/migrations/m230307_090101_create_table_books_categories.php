<?php

use yii\db\Migration;

/**
 * Class m230307_090101_create_table__books_categories
 */
class m230307_090101_create_table_books_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('books_categories', [
            'book_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('books_categories',
            'books_categories',
            ['book_id', 'category_id']);

        $this->addForeignKey(
            'fk_bookID2',
            'books_categories',
            'book_id',
            'books',
            'id',
            'CASCADE',
            'CASCADE',
        );

        $this->addForeignKey(
            'fk_categoryID',
            'books_categories',
            'category_id',
            'categories',
            'id',
            'CASCADE',
            'CASCADE',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_categoryID', 'books_categories');
        $this->dropForeignKey('fk_bookID2', 'books_categories');
        $this->dropPrimaryKey('books_categories', 'books_categories');
        $this->dropTable('books_categories');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230307_090101_create_table_books_categories cannot be reverted.\n";

        return false;
    }
    */
}
