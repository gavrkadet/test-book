<?php

declare(strict_types=1);

namespace console\controllers;

use common\models\BooksCategories;
use CURLFile;
use ErrorException;
use Throwable;
use yii\console\Controller;
use common\models\Book;
use common\models\Category;
use common\models\Author;
use common\models\Status;
use common\models\BooksAuthors;


class ParserController extends Controller
{
    const STRING_FILE_NAME = 'https://gitlab.com/prog-positron/test-app-vacancy/-/raw/master/books.json';

    public function actionIndex()
    {
        $stringContentFile = file_get_contents(ParserController::STRING_FILE_NAME);
        if ($stringContentFile) {
            $result = json_decode($stringContentFile, true);
            $data = $this->returnData($result);
            foreach ($data as $book) {
                $bookModel = new Book();
                $bookModel->title = $book['title'] ?? '';
                $bookModel->isbn = $book['isbn'] ?? '';
                $bookModel->pageCount = $book['pageCount'] ?? '';
                $bookModel->publishedDate = $book['publishedDate'] ?? '';
                $bookModel->thumbnailUrl = $book['thumbnailUrl'] ?? '';
                $bookModel->shortDescription = $book['shortDescription'] ?? '';
                $bookModel->longDescription = $book['longDescription'] ?? '';
                $IDStatus = Status::find()
                    ->where(['name' => $book['status']])
                    ->one()['id'];
                $bookModel->status_id = $IDStatus ?? '';
                $bookSearchModel = Book::find()
                    ->where(['title' => $bookModel->title])
                    ->one();
                if (!$bookSearchModel) {
                    if (!$bookModel->save()) {
                        dump('Книга с заголовком ' . $bookModel->title . ' не сохранилась в базе');
                    }
                }
                $this->saveBooksAuthors($book);
                if ($book['categories'] !== []) {
                    $this->saveBooksCategories($book);
                } else{
                    $this->saveBooksCategoriesNew($book);
                }
            }
        } else {
            dump('Файл не найден');
        }
    }

    public function actionAuthors()
    {//Тут мы получим всех авторов из файла books.json и запишем их в таблицу books_authors
        $stringContentFile = file_get_contents(ParserController::STRING_FILE_NAME);
        if ($stringContentFile) {
            $result = json_decode($stringContentFile, true);
            $data = [];
            foreach ($result as $book) {
                try {
                    $data[] = [
                        'authors' => $book['authors'] ?? '',
                    ];
                } catch (ErrorException $e) {
                    dump($e);
                }
            }// Формируем уникальный массив со всеми авторами
            $finishArrayNamesAuthors = $this->generateFinishArrayAuthors($data);
            foreach ($finishArrayNamesAuthors as $author) {
                $model = new Author();
                $model->name = $author;
                $searchModel = Author::find()
                    ->where(['name' => $model->name])
                    ->one();
                if (!$searchModel) {        // Если такого автора еще нет,
                    if (!$model->save()) {  // записать в таблицу
                        dump('Ошибка! Автор не добавлен в базу!');
                    }
                }
            }
        } else {
            dump('Файл не найден');
        }
    }

    public function actionCategories()
    {//Тут мы получим все категории из файла books.json и запишем их в таблицу books_categories
        $stringContentFile = file_get_contents(ParserController::STRING_FILE_NAME);
        if ($stringContentFile) {
            $result = json_decode($stringContentFile, true);
            $data = [];
            foreach ($result as $book) {
                try {
                    $data[] = [
                        'categories' => array_diff($book['categories'], ['']) ?? '',
                    ];
                } catch (ErrorException $e) {
                    dump($e);
                }
            }// Формируем уникальный массив со всеми категориями
            $finishArrayNamesCategories = $this->generateFinishArrayCategories($data);
            foreach ($finishArrayNamesCategories as $category) {
                    $model = new Category();
                    $model->name = $category;
                    $searchModel = Category::find()
                        ->where(['name' => $model->name])
                        ->one();
                    if (!$searchModel) {        // Если такой категории еще нет,
                        if (!$model->save()) {  // записать в таблицу
                            dump('Ошибка! Категория не добавлена в базу!');
                        }
                    }
                }
            $this->addCategoryNew();
        } else {
            dump('Файл не найден');
        }
    }

    public function actionStatuses()
    {
        $stringContentFile = file_get_contents(ParserController::STRING_FILE_NAME);
        if ($stringContentFile) {
            $result = json_decode($stringContentFile, true);
            $data = [];
            foreach ($result as $book) {
                try {
                    $data[] = [
                        'status' => $book['status'] ?? '',
                    ];
                } catch (ErrorException $e) {
                    dump($e);
                }
            }// Формируем уникальный массив со всеми статусами
            $finishArrayStatuses = $this->generateFinishArrayStatuses($data);
            foreach ($finishArrayStatuses as $status) {
                $model = new Status();
                $model->name = $status;
                $searchModel = Status::find()
                    ->where(['name' => $model->name])
                    ->one();
                if (!$searchModel) {        // Если такого статуса еще нет,
                    if (!$model->save()) {  // записать в таблицу
                        dump('Ошибка! Статус не добавлен в базу!');
                    }
                }
            }
        }
    }

    public function actionImages()
    {
        $stringContentFile = file_get_contents(ParserController::STRING_FILE_NAME);
        if ($stringContentFile) {
            $result = json_decode($stringContentFile, true);
            $data = [];
            foreach ($result as $book) {
                try {
                    $data[] = [
                        'thumbnailUrl' => $book['thumbnailUrl'] ?? '',
                    ];
                } catch (ErrorException $e) {
                    dump($e);
                }
            }
            $finishArrayImages = $this->generateFinishArrayImages($data);
            $this->downloadImages($finishArrayImages);
        }
    }

    public function addCategoryNew()
    {
        $model = new Category();
        $model->name = 'Новинки';
            $searchModel = Category::find()
                ->where(['name' => $model->name])
                ->one();

        if (!$searchModel) {
            if (!$model->save()) {
                dump('Категория Новинки не добавилась');
            }
        }
    }

    public function saveBooksCategories($book)
    {
        foreach ($book['categories'] as $category) {
            if (!empty($category)) {
                $IDCategory = Category::find()
                    ->where(['name' => $category])
                    ->one()['id'];
                $IDBook = Book::find()
                    ->where(['title' => $book['title']])
                    ->one()['id'];
                $model = new BooksCategories();
                if ($IDBook and $IDCategory) {
                    $model->book_id = $IDBook;
                    $model->category_id = $IDCategory;
                } else {
                    dump('Ошибка! Категория не найдена');
                }
                $modelSearch = BooksCategories::find()
                    ->where(['book_id' => $model->book_id])
                    ->andWhere(['category_id' => $model->category_id])
                    ->one();
                if (!$modelSearch) {
                    if (!$model->save()) {
                        dump('Ошибка. Данные не записались в таблицу books_categories');
                    }
                }
            }

        }
    }

    public function saveBooksCategoriesNew($book)
    {
        $IDCategory = Category::find()
            ->where(['name' => 'Новинки'])
            ->one()['id'];
        $IDBook = Book::find()
            ->where(['title' => $book['title']])
            ->one()['id'];
        $model = new BooksCategories();
        if ($IDBook and $IDCategory) {
            $model->book_id = $IDBook;
            $model->category_id = $IDCategory;
        } else {
            dump('Ошибка! Категория не найдена');
        }
        $modelSearch = BooksCategories::find()
            ->where(['book_id' => $model->book_id])
            ->andWhere(['category_id' => $model->category_id])
            ->one();
        if (!$modelSearch) {
            if (!$model->save()) {
                dump('Ошибка. Данные не записались в таблицу books_categories');
            }
        }
    }

    public function saveBooksAuthors($book)
    {
        foreach ($book['authors'] as $author) {
            if (!empty($author)) {
                $IDAuthor = Author::find()
                    ->where(['name' => $author])
                    ->one()['id'];
                $IDBook = Book::find()
                    ->where(['title' => $book['title']])
                    ->one()['id'];
                $model = new BooksAuthors();
                if ($IDBook and $IDAuthor) {
                    $model->book_id = $IDBook;
                    $model->author_id = $IDAuthor;
                } else {
                    dump('Ошибка! Автор не найден!');
                }
                $modelSearch = BooksAuthors::find()
                    ->where(['book_id' => $model->book_id])
                    ->andWhere(['author_id' => $model->author_id])
                    ->one();
                if (!$modelSearch) {
                    if (!$model->save()) {
                        dump('Ошибка. Данные не записались в таблицу books_authors');
                    }
                }
            }
        }
    }

    public function returnData($result): array
    {
        $data = [];
        foreach ($result as $book) {
            try {
                $data[] = [
                    'title' => $book['title'] ?? '',
                    'isbn' => $book['isbn'] ?? '',
                    'pageCount' => $book['pageCount'] ?? '',
                    'publishedDate' => $book['publishedDate']['$date'] ?? '',
                    'thumbnailUrl' => $book['thumbnailUrl'] ?? '',
                    'shortDescription' => $book['shortDescription'] ?? '',
                    'longDescription' => $book['longDescription'] ?? '',
                    'status' => $book['status'] ?? '',
                    'authors' => array_diff($book['authors'], ['']) ?? '',
                    'categories' => $book['categories'] ?? '',
                ];
            } catch (ErrorException $e) {
                dump($e);
            }
        }
        return $data;
    }

    public function generateFinishArrayImages($data): array
    {
        $arrayImages = [];
        for ($i = 0; $i < count($data); $i++) {
            $arrayImages[] = $data[$i]['thumbnailUrl'] ?? '';
        }// Формируем уникальный массив со всеми авторами
        return $arrayImages;
    }

    public function downloadImages($finishArrayImages)
    {
        foreach ($finishArrayImages as $image) {
            if ($image !== '') {
                $path = parse_url($image, PHP_URL_PATH);
                if ($path) {
                    $filename = basename($path);
                    $imagesDirectory = __DIR__ . '\..\..\frontend\web\src\\';
                    $image_local = tempnam($imagesDirectory, 'FOO');
                    file_put_contents($imagesDirectory . $filename, file_get_contents($image));
                    $url['photo'] = new CURLFile($image_local, 'image/jpeg', 'image.jpg');
                    sleep(2);
                    $ch = curl_init($image_local);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $url);
                    curl_close($ch);
                    unset($url['photo']);
                    unlink($image_local);
                }
            }
        }
    }

    public function generateFinishArrayCategories($data): array
    {
        $arrayNamesCategories = [];
        for ($i = 0; $i < count($data); $i++) {
            for ($j = 0; $j < count($data[$i]['categories']); $j++) {
                if (!empty($data[$i]['categories'][$j])) {
                    $arrayNamesCategories[] = $data[$i]['categories'][$j];
                }
            }

        }// Формируем уникальный массив со всеми категориями
        $arrayUniqueNamesCategories = array_unique($arrayNamesCategories);
        return array_values($arrayUniqueNamesCategories);
    }

    public function generateFinishArrayAuthors($data): array
    {
        $arrayNamesAuthors = [];
        for ($i = 0; $i < count($data); $i++) {
            for ($j = 0; $j < count($data[$i]['authors']); $j++) {
                $arrayNamesAuthors[] = $data[$i]['authors'][$j] ?? '';
            }
        }// Формируем уникальный массив со всеми авторами
        $arrayUniqueNamesAuthors = array_unique($arrayNamesAuthors);
        return (array_diff(array_values($arrayUniqueNamesAuthors), ['']));
    }

    public function generateFinishArrayStatuses($data): array
    {
        $arrayNamesStatuses = [];
        for ($i = 0; $i < count($data); $i++) {
            if (!empty($data[$i]['status'])) {
                $arrayNamesStatuses[] = $data[$i]['status'];
            }
        }// Формируем уникальный массив со всеми авторами
        $arrayUniqueNamesStatuses = array_unique($arrayNamesStatuses);
        return array_values($arrayUniqueNamesStatuses);
    }
}
