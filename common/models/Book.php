<?php

namespace common\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property string $title
 * @property string|null $isbn
 * @property int $pageCount
 * @property string|null $publishedDate
 * @property string|null $thumbnailUrl
 * @property string|null $shortDescription
 * @property string|null $longDescription
 * @property int $status_id
 *
 * @property Author[] $authors
 * @property Category[] $categories
 * @property BooksAuthors[] $booksAuthors
 * @property BooksCategories[] $booksCategories
 * @property Status $status
 */
class Book extends \yii\db\ActiveRecord
{
    public string $strCategories = '';
    public string $strAuthors = '';
    const STATUS_DRAFT = 0;

    const STATUS_ACTIVE = 1;

    const PAGE_SIZE = 10;
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['title', 'pageCount', 'status_id'], 'required'],
            [['pageCount', 'status_id'], 'integer'],
            [['shortDescription', 'longDescription', 'isbn', 'strAuthors', 'strCategories'], 'string'],
            [['title', 'publishedDate', 'thumbnailUrl'], 'string', 'max' => 255],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::class, 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'isbn' => 'Артикул',
            'pageCount' => 'Количество страниц',
            'publishedDate' => 'Дата публикации',
            'thumbnailUrl' => 'Ссылка на картинку',
            'shortDescription' => 'Краткое описание',
            'longDescription' => 'Полное описание',
            'status_id' => 'Статус',
            'strAuthors' => 'Авторы',
            'strCategories' => 'Категории',
        ];
    }

    /**
     * Gets query for [[Authors]].
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getAuthors(): ActiveQuery
    {
        return $this->hasMany(Author::class, ['id' => 'author_id'])
            ->viaTable('books_authors', ['book_id' => 'id']);
    }

    /**
     * Gets query for [[BooksAuthors]].
     *
     * @return ActiveQuery
     */
    public function getBooksAuthors(): ActiveQuery
    {
        return $this->hasMany(BooksAuthors::class, ['book_id' => 'id']);
    }

    /**
     * Gets query for [[BooksCategories]].
     *
     * @return ActiveQuery
     */
    public function getBooksCategories(): ActiveQuery
    {
        return $this->hasMany(BooksCategories::class, ['book_id' => 'id']);
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return ActiveQuery
     */
    public function getCategories(): ActiveQuery
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])
            ->viaTable('books_categories', ['book_id' => 'id']);
    }

    /**
     * Gets query for [[Status]].
     *
     * @return ActiveQuery
     */
    public function getStatus(): ActiveQuery
    {
        return $this->hasOne(Status::class, ['id' => 'status_id']);
    }

    public function getStringAllAuthors($model): string
    {
        $arrayAuthors = [];
        foreach ($model->authors as $author) {
            $arrayAuthors[] = $author->name;
        }
        return implode(', ', $arrayAuthors);
    }

    public function getStringAllCategories($model): string
    {
        $arrayCategories = [];
        foreach ($model->categories as $category) {
            $arrayCategories[] = $category->name;
        }
       return implode(', ', $arrayCategories);
    }

    public function getArrayStatuses(): array
    {
        return Status::find()->all();
    }
}
