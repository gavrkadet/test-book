up:
	docker compose up --force-recreate --remove-orphans --build # --detach
up-detach:
	docker compose up --force-recreate --remove-orphans --build --detach
php-sh:
	docker compose exec php-fpm sh
nginx-sh:
	docker compose exec nginx sh
db-sh:
	docker compose exec db sh
frontend-sh:
	docker compose exec frontend sh
backend-sh:
	docker compose exec backend sh
