<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\BookCategories $model */

$this->title = 'Create Book Categories';
$this->params['breadcrumbs'][] = ['label' => 'Book Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
