<?php

use common\models\Book;
use common\models\Status;
use mranger\load_more_pager\LoadMorePager;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var backend\models\BookSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var common\models\Book $model */

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'test-book',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
         'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'isbn',
            'pageCount',
            'publishedDate',
            [
                'attribute' => 'status_id',
                'value' => function (Book $model) {
                    return $model->status->name;
                }
            ],
            [
                'attribute' => 'strAuthors',
                'value' => function (Book $model) {
                    return $model->getStringAllAuthors($model);
                }
            ],
            [
                'attribute' => 'strCategories',
                'value' => function (Book $model) {
                    return $model->getStringAllCategories($model);
                }
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{view}',
                'urlCreator' => function ($action, Book $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>
