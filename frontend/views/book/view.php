<?php

use common\models\Book;
use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var common\models\Book $model */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Книги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="book-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function (Book $model) {
                    $path = parse_url($model->thumbnailUrl, PHP_URL_PATH);
                    if ($path) {
                        $filename = basename($path);
                        return Html::img('http://test-book/src/' . $filename, [
                            'height' => '200',
                            'width' => '200',
                        ]);
                    }
                }
            ],
            'title',
            [
                'attribute' => 'strCategories',
                'value' => function (Book $model) {
                    return $model->getStringAllCategories($model);
                }
            ],
            'pageCount',
            'isbn',
        ],
    ]) ?>

</div>
